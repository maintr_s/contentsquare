Ce projet consiste a mocker un programme de lancement de Job Spark a la suite de la reception d'un message sur une queue (comme par Exemple avec RabbitMQ).

Ici les jobs spark sont remplacés par l'execution du script job.sh
Et la queue est ici remplacé par des fichiers que l'ont doit inserer dans un dossier observé

## Requirements

Python 3.4

## Installation

sudo apt-get install python3.4

pip install watchdog

## Run

python3.4 programme.py [dossier à watch]
