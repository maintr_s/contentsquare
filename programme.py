import sys
import os
import subprocess
import queue
import threading
import time

from Task import *
from ColorText import *
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

num_threads = 3
q = queue.Queue()

class EventHandler(FileSystemEventHandler):
    def __init__(self, directory):
        self.directory = directory

    def __get_id(self, name):
        param = name.split(":")
        if len(param) >= 2:
            q.put(Task(param[1]))
            return 0
        return 1

    def process(self, event):
        if event.is_directory == 0:
            file = os.path.basename(event.src_path)
            if self.__get_id(file) == 0:
                os.remove(event.src_path)

    def on_created(self, event):
        self.process(event)

    def on_moved(self, event):
        self.process(event)

    def on_modified(self, event):
        self.process(event)



def execute_job(task):
    p = subprocess.Popen(['./job.sh', task.id])
    p.communicate()
    if p.returncode == 0:
        good("Job " + str(task.id) + " succeed")
    elif task.nb_try < 3:
        warning("Job " + str(task.id) + " fail, retry left: " + str(3 - task.nb_try))
        task.nb_try += 1
        q.put(task)
    else:
        drop("Job " + str(task.id) + " failed too many times")


def process():
    while 1:
        item = q.get()
        execute_job(item)


def set_threading():
    pool = [threading.Thread(target=process) for i in range(num_threads)]
    for t in pool:
        t.daemon = True
        t.start()



def main():
    argv = sys.argv[1:]
    directory = argv[0]

    set_threading()
    event_handler = EventHandler(directory)

    observer = Observer()
    observer.schedule(event_handler, directory, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


if __name__ == '__main__':
    main()
