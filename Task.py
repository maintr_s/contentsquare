class Task:
    def __init__(self, id):
        self.id = id
        self.nb_try = 0

    def __repr__(self):
        return "Task={id:%s, nbtry:%s}" % (self.id, self.nb_try)